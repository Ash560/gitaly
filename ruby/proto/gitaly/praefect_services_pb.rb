# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: praefect.proto for package 'gitaly'

require 'grpc'
require 'praefect_pb'

module Gitaly
  module InfoService
    class Service

      include GRPC::GenericService

      self.marshal_class_method = :encode
      self.unmarshal_class_method = :decode
      self.service_name = 'gitaly.InfoService'

      rpc :ListRepositories, ListRepositoriesRequest, stream(ListRepositoriesResponse)
    end

    Stub = Service.rpc_stub_class
  end
end
